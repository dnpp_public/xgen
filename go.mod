module github.com/xuri/xgen

go 1.15

require (
	github.com/antchfx/xmlquery v1.3.15
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-xmlfmt/xmlfmt v1.1.2
	github.com/moovweb/gokogiri v0.0.0-20180713195410-a1a828153468
	github.com/stretchr/testify v1.7.1
	golang.org/x/net v0.7.0
	gopkg.in/yaml.v3 v3.0.0 // indirect
)
