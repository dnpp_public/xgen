// Copyright 2020 - 2021 The xgen Authors. All rights reserved. Use of this
// source code is governed by a BSD-style license that can be found in the
// LICENSE file.
//
// Package xgen written in pure Go providing a set of functions that allow you
// to parse XSD (XML schema files). This library needs Go version 1.10 or
// later.

package xgen

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/antchfx/xmlquery"
	xmlTool "github.com/antchfx/xmlquery"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var (
	testFixtureDir = "test"
	// externalFixtureDir is where one copy their own XSDs to run validation on them. For a set
	// of XSDs to run tests on, see https://github.com/xuri/xsd. Note that external tests leave the
	// generated output for inspection to support use-cases of manual review of generated code
	externalFixtureDir = "data"
)

func TestParseGoSpark(t *testing.T) {
	testParseForSpark(t, "Go", "go", "go", testFixtureDir, true)
}

func testParseForSpark(t *testing.T, lang string, fileExt string, langDirName string, sourceDirectory string, leaveOutput bool) {
	codeDir := filepath.Join(sourceDirectory, langDirName)

	outputDir := filepath.Join(codeDir, "output")
	if leaveOutput {
		err := PrepareOutputDir(outputDir)
		require.NoError(t, err)
	} else {
		tempDir, err := ioutil.TempDir(codeDir, "output-*")
		require.NoError(t, err)
		defer os.RemoveAll(tempDir)

		outputDir = tempDir
	}

	projectName := "spark"

	inputDir := filepath.Join(sourceDirectory, projectName)

	splitFolder := filepath.Join(inputDir, "split_files")
	if err := os.MkdirAll(splitFolder, 0755); err != nil {
		log.Fatal(err)
	}

	xsd_files, err := os.ReadDir(inputDir)

	SourceFileMap := make(map[string]string)

	for _, v := range xsd_files {
		/*		if v.Name() == `ArchivedResult.xsd` ||
				v.Name() == `Classifier.xsd` ||
				v.Name() == `CommonTypes.xsd` {
				continue
			}*/
		SplitXSD_subEle(inputDir+`/`+v.Name(), splitFolder, strings.ReplaceAll(v.Name(), ".xsd", ""), SourceFileMap)
	}

	files, err := os.ReadDir(splitFolder)
	if err != nil {
		fmt.Println(err)
		return
	}

	for k, fileItem := range files {

		file := splitFolder + `/` + fileItem.Name()

		pr := float32((k * 100) / len(files))
		log.Printf(`FileName:=%s  done: %.2f`, file, pr)

		name := filepath.Base(file)
		ext := filepath.Ext(name)
		check_path := `xmlGenerated_` + projectName + `\` + name[0:len(name)-len(ext)] + `\` + name[0:len(name)-len(ext)] + `.xml`
		if _, err := os.Stat(check_path); !os.IsNotExist(err) {
			continue
		}

		if filepath.Ext(file) == ".xsd" {
			parser := NewParser(&Options{
				FilePath:            file,
				InputDir:            inputDir,
				OutputDir:           outputDir,
				Lang:                lang,
				IncludeMap:          make(map[string]bool),
				LocalNameNSMap:      make(map[string]string),
				NSSchemaLocationMap: make(map[string]string),
				ParseFileList:       make(map[string]bool),
				ParseFileMap:        make(map[string][]interface{}),
				ProtoTree:           make([]interface{}, 0),
				ReplaceTypeMap:      make(map[TypeItem]NodeItem),
				IsRootTagExport:     false,
				ProjectName:         projectName,
				MainFile:            SourceFileMap[fileItem.Name()],
			})
			methodName := strings.ReplaceAll(fileItem.Name(), ".xsd", "")
			err = parser.Parse(true, inputDir, methodName)
			assert.NoError(t, err, file)
		}
	}
}

func TestParseGoFz223(t *testing.T) {
	testParseForFz223(t, "Go", "go", "go", testFixtureDir, true)
}

func TestParseGoFz44(t *testing.T) {
	//testParseForFz44(t, "Go", "go", "go", testFixtureDir, true)
}

func SplitXSD(path, folder string) {

	xmlFile, err := os.Open(path)
	if err != nil {
		fmt.Println(err)
	}
	defer xmlFile.Close()
	t, err := xmlTool.Parse(xmlFile)
	if err != nil {
		return
	}

	list := xmlquery.Find(t, "//xs:choice")
	if len(list) > 0 {
		eleList := list[0].SelectElements("xs:element")

		for _, v := range list[0].SelectElements("xs:element") {
			xmlTool.RemoveFromTree(v)
		}

		for _, v := range eleList {
			xmlTool.AddChild(list[0], v)

			err = ioutil.WriteFile(folder+`/`+v.SelectAttr(`name`)+`.xsd`, []byte(t.OutputXML(true)), 0644)
			if err != nil {
				log.Fatal(err)
			}

			xmlTool.RemoveFromTree(v)
		}
	}
}

func SplitXSD_subEle(path, folder, name string, mainFile map[string]string) {

	xmlFile, err := os.Open(path)
	if err != nil {
		fmt.Println(err)
	}
	defer xmlFile.Close()
	t, err := xmlTool.Parse(xmlFile)
	if err != nil {
		return
	}

	list := xmlquery.Find(t, "//xs:schema")

	if len(list) > 0 {
		eleList := list[0].SelectElements("xs:element")

		//Remove all children
		iterator := list[0].FirstChild
		for iterator != nil {
			temp := iterator
			iterator = iterator.NextSibling
			xmlTool.RemoveFromTree(temp)
		}

		for _, v := range eleList {
			xmlTool.AddChild(list[0], v)

			err = ioutil.WriteFile(folder+`/`+name+`_`+v.SelectAttr(`name`)+`.xsd`, []byte(t.OutputXML(true)), 0644)
			if err != nil {
				log.Fatal(err)
			}

			mainFile[name+`_`+v.SelectAttr(`name`)+`.xsd`] = path

			xmlTool.RemoveFromTree(v)
		}
	}
}

// TestParseGoExternal runs tests on any external XSDs within the externalFixtureDir
// func TestParseGoExternal(t *testing.T) {
// 	testParseForSource(t, "Go", "go", "go", externalFixtureDir, true)
// }

// testParseForSource runs parsing tests for a given language. The sourceDirectory specifies the root of the
// input for the tests. The expected structure of the sourceDirectory is as follows:
//
//	source
//	├── xsd (with the input xsd files to run through the parser)
//	└── <langDirName> (with the expected generated code named <xsd-file>.<fileExt>
//
// The test cleans up files it generates unless leaveOutput is set to true. In which case, the generate file is left
// on disk for manual inspection under <sourceDirectory>/<langDirName>/output.
func testParseForFz44(t *testing.T, lang string, fileExt string, langDirName string, sourceDirectory string, leaveOutput bool) {
	codeDir := filepath.Join(sourceDirectory, langDirName)

	outputDir := filepath.Join(codeDir, "output")
	if leaveOutput {
		err := PrepareOutputDir(outputDir)
		require.NoError(t, err)
	} else {
		tempDir, err := ioutil.TempDir(codeDir, "output-*")
		require.NoError(t, err)
		defer os.RemoveAll(tempDir)

		outputDir = tempDir
	}

	inputDir := filepath.Join(sourceDirectory, "fz44")
	// files, err := GetFileList(inputDir)
	// // Abort testing if the source directory doesn't include a xsd directory with inputs
	// if os.IsNotExist(err) {
	// 	return
	// }

	//require.NoError(t, err)
	file := `test\fz44\fcsExport.xsd`

	splitFolder := filepath.Join(inputDir, "split_files")
	if err := os.MkdirAll(splitFolder, 0755); err != nil {
		log.Fatal(err)
	}

	SplitXSD(file, splitFolder)

	files, err := os.ReadDir(splitFolder)
	if err != nil {
		fmt.Println(err)
		return
	}

	for k, fileItem := range files {

		file := splitFolder + `/` + fileItem.Name()

		pr := float32((k * 100) / len(files))
		log.Printf(`FileName:=%s  done: %.2f`, file, pr)

		name := filepath.Base(file)
		ext := filepath.Ext(name)
		check_path := `xmlGenerated\` + name[0:len(name)-len(ext)] + `\` + name[0:len(name)-len(ext)] + `.xml`
		if _, err := os.Stat(check_path); !os.IsNotExist(err) {
			continue
		}

		if filepath.Ext(file) == ".xsd" {
			xsdName, err := filepath.Rel(inputDir, file)
			require.NoError(t, err)

			t.Run(xsdName, func(t *testing.T) {
				parser := NewParser(&Options{
					FilePath:            file,
					InputDir:            inputDir,
					OutputDir:           outputDir,
					Lang:                lang,
					IncludeMap:          make(map[string]bool),
					LocalNameNSMap:      make(map[string]string),
					NSSchemaLocationMap: make(map[string]string),
					ParseFileList:       make(map[string]bool),
					ParseFileMap:        make(map[string][]interface{}),
					ProtoTree:           make([]interface{}, 0),
					ReplaceTypeMap:      make(map[TypeItem]NodeItem),
					IsRootTagExport:     true,
					ProjectName:         "fz44",
				})
				err = parser.Parse(true, inputDir, "")
				assert.NoError(t, err, file)
				//generatedFileName := strings.TrimPrefix(file, inputDir) + "." + fileExt
				//actualFilename := filepath.Join(outputDir, generatedFileName)

				// actualGenerated, err := ioutil.ReadFile(actualFilename)
				// assert.NoError(t, err)

				// expectedFilename := filepath.Join(codeDir, generatedFileName)
				// expectedGenerated, err := ioutil.ReadFile(expectedFilename)
				// assert.NoError(t, err)

				// assert.Equal(t, string(expectedGenerated), string(actualGenerated), fmt.Sprintf("error in generated code for %s", file))
			})
		}
	}
}

func testParseForFz223(t *testing.T, lang string, fileExt string, langDirName string, sourceDirectory string, leaveOutput bool) {
	codeDir := filepath.Join(sourceDirectory, langDirName)

	outputDir := filepath.Join(codeDir, "output")
	if leaveOutput {
		err := PrepareOutputDir(outputDir)
		require.NoError(t, err)
	} else {
		tempDir, err := ioutil.TempDir(codeDir, "output-*")
		require.NoError(t, err)
		defer os.RemoveAll(tempDir)

		outputDir = tempDir
	}

	projectName := "fz223"

	inputDir := filepath.Join(sourceDirectory, projectName)

	splitFolder := filepath.Join(inputDir, "split_files")
	if err := os.MkdirAll(splitFolder, 0755); err != nil {
		log.Fatal(err)
	}

	xsd_files, err := os.ReadDir(inputDir)

	SourceFileMap := make(map[string]string)

	for _, v := range xsd_files {
		if v.Name() == `Types.xsd` ||
			v.Name() == `webRequest.xsd` ||
			v.Name() == `ecp-packet.xsd` ||
			v.Name() == `ecp-packetSpec.xsd` {
			continue
		}
		SplitXSD_subEle(inputDir+`/`+v.Name(), splitFolder, v.Name(), SourceFileMap)
	}

	files, err := os.ReadDir(splitFolder)
	if err != nil {
		fmt.Println(err)
		return
	}

	for k, fileItem := range files {

		file := splitFolder + `/` + fileItem.Name()

		pr := float32((k * 100) / len(files))
		log.Printf(`FileName:=%s  done: %.2f`, file, pr)

		name := filepath.Base(file)
		ext := filepath.Ext(name)
		check_path := `xmlGenerated_` + projectName + `\` + name[0:len(name)-len(ext)] + `\` + name[0:len(name)-len(ext)] + `.xml`
		if _, err := os.Stat(check_path); !os.IsNotExist(err) {
			continue
		}

		if filepath.Ext(file) == ".xsd" {
			xsdName, err := filepath.Rel(inputDir, file)
			require.NoError(t, err)

			t.Run(xsdName, func(t *testing.T) {
				parser := NewParser(&Options{
					FilePath:            file,
					InputDir:            inputDir,
					OutputDir:           outputDir,
					Lang:                lang,
					IncludeMap:          make(map[string]bool),
					LocalNameNSMap:      make(map[string]string),
					NSSchemaLocationMap: make(map[string]string),
					ParseFileList:       make(map[string]bool),
					ParseFileMap:        make(map[string][]interface{}),
					ProtoTree:           make([]interface{}, 0),
					ReplaceTypeMap:      make(map[TypeItem]NodeItem),
					IsRootTagExport:     false,
					ProjectName:         projectName,
					MainFile:            SourceFileMap[fileItem.Name()],
				})
				err = parser.Parse(true, inputDir, "")
				assert.NoError(t, err, file)
			})
		}
	}
}
