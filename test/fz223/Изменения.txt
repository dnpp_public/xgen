1. typalOrderClause.xsd
	1.1. Изменен тип typalOrderClause (Типовое положение о закупке товаров, работ, услуг)
		1.1.1. Добавлен элемент notPlacement (Признак сокрытия документа) в блоке attachments
2. OrderClause.xsd
	2.1 Изменен тип OrderClause (Положение о закупке товаров, работ, услуг)
		2.1.1. Добавлен элемент notPlacement (Признак сокрытия документа) в блоке attachments
3. ecp-packet.xsd
	3.1 Добавлен тип indicationOpenRZ2ZPESMBO (Сведения о признаке вскрытия вторых частей заявок)