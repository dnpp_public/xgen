// Copyright 2020 - 2021 The xgen Authors. All rights reserved. Use of this
// source code is governed by a BSD-style license that can be found in the
// LICENSE file.
//
// Package xgen written in pure Go providing a set of functions that allow you
// to parse XSD (XML schema files). This library needs Go version 1.10 or
// later.

package xgen

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"reflect"
	"strings"

	"github.com/antchfx/xmlquery"
	xmlTool "github.com/antchfx/xmlquery"

	"golang.org/x/net/html/charset"
)

func isPanic(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

type TypeItem struct {
	FileName string
	Type     string
}

type NodeItem struct {
	Node     *xmlTool.Node
	NodeType string
}

// Options holds user-defined overrides and runtime data that are used when
// parsing from an XSD document.
type Options struct {
	FilePath            string
	FileDir             string
	InputDir            string
	OutputDir           string
	Extract             bool
	Lang                string
	Package             string
	IncludeMap          map[string]bool
	LocalNameNSMap      map[string]string
	NSSchemaLocationMap map[string]string
	ParseFileList       map[string]bool
	ParseFileMap        map[string][]interface{}
	ProtoTree           []interface{}
	RemoteSchema        map[string][]byte
	ReplaceTypeMap      map[TypeItem]NodeItem

	InElement        string
	CurrentEle       string
	InGroup          int
	InUnion          bool
	InAttributeGroup bool
	IsRootTagExport  bool
	ProjectName      string
	MainFile         string

	SimpleType     *Stack
	ComplexType    *Stack
	Element        *Stack
	Attribute      *Stack
	Group          *Stack
	AttributeGroup *Stack
	Choice         *Stack
	Restrictions   *Stack
}

// NewParser creates a new parser options for the Parse. Useful for XML schema
// parsing.
func NewParser(options *Options) *Options {
	return options
}

func GetBase(src *xmlTool.Node) *xmlTool.Node {
	extBaseNode := src.SelectElement("xs:sequence")

	if extBaseNode == nil {
		extBaseNode = src.SelectElement("xs:choice")
	}

	if extBaseNode == nil {
		extBaseNode = src.SelectElement("xs:group")
	}

	if extBaseNode == nil {
		extBaseNode = src.SelectElement("xs:all")
	}

	return extBaseNode
}

func FindNodeByName(path, name string) (*xmlTool.Node, string, error) {
	xmlFile, err := os.Open(path)
	if err != nil {
		return nil, "", err
	}
	defer xmlFile.Close()

	t, err := xmlTool.Parse(xmlFile)
	if err != nil {
		return nil, "", err
	}

	expr := "//xs:complexType[@name='" + name + "']"
	list := xmlquery.Find(t, expr)
	if len(list) > 0 {
		return list[0], "complexType", nil
	}

	expr = "//xs:simpleType[@name='" + name + "']"
	list = xmlquery.Find(t, expr)
	if len(list) > 0 {
		return list[0], "simpleType", nil
	}

	return nil, "", nil
}

func (opt *Options) WalkXML(t *xmlTool.Node, schemaLocationParent, parentType string) {
	list := xmlquery.Find(t, "//xs:element/@type")
	attrList := xmlquery.Find(t, "//xs:attribute/@type")
	extensionList := xmlquery.Find(t, "//xs:extension/@base")
	list = append(list, attrList...)
	list = append(list, extensionList...)

	for _, item := range list {
		if item.Parent != nil {
			for _, v := range item.Parent.Attr {
				if strings.ToLower(v.Name.Local) == `type` || strings.ToLower(v.Name.Local) == `base` {

					isExtension := strings.ToLower(v.Name.Local) == `base`
					isBuildIn := false

					var subNode *xmlTool.Node
					nodeTy := ""

					value := v.Value
					if !isExtension {
						if _, ok := getBuildInTypeByLang(trimNSPrefix(value), opt.Lang); ok {
							continue
						}
					} else {
						if typeName, ok := getBuildInTypeByLang(trimNSPrefix(value), opt.Lang); ok {
							isBuildIn = true
							nodeTy = "simpleType"
							value = typeName
						}
					}

					schemaLocation := opt.NSSchemaLocationMap[opt.parseNS(value)]

					if !(isExtension && isBuildIn) {
						//schemaLocation := opt.NSSchemaLocationMap[opt.parseNS(value)]
						if schemaLocation == "" && schemaLocationParent != "" {
							schemaLocation = schemaLocationParent
						}

						xsdFile := filepath.Join(opt.FileDir, schemaLocation)
						var fi os.FileInfo
						fi, err := os.Stat(xsdFile)
						isPanic(err)

						if !fi.IsDir() {
							subNode, nodeTy, err = FindNodeByName(xsdFile, trimNSPrefix(value))
							isPanic(err)
						}

						if subNode == nil {
							//Look up in source file firstly
							subNode, nodeTy, err = FindNodeByName(opt.MainFile, trimNSPrefix(value))
							isPanic(err)
						}

						if subNode == nil {
							subNode, nodeTy, err = FindNodeByName(filepath.Join(opt.FileDir, `CommonTypes.xsd`), trimNSPrefix(value))
							if !os.IsNotExist(err) {
								isPanic(err)
							}
						}

						if subNode == nil {
							subNode, nodeTy, err = FindNodeByName(filepath.Join(opt.FileDir, `BaseTypes.xsd`), trimNSPrefix(value))
							if !os.IsNotExist(err) {
								isPanic(err)
							}
						}

						if subNode == nil {
							xsd_files, err := os.ReadDir(opt.FileDir)
							isPanic(err)

							for _, v := range xsd_files {
								subNode, nodeTy, err = FindNodeByName(filepath.Join(opt.FileDir, v.Name()), trimNSPrefix(value))
								isPanic(err)
								if subNode != nil {
									break
								}
							}
						}

						if subNode == nil {
							log.Fatal(fmt.Errorf(`No type found`))
						}
					}

					if subNode != nil {
						//Prevent infinite recurrsion
						isInfiniteLoop := parentType == subNode.SelectAttr("name")
						if !isInfiniteLoop {
							opt.WalkXML(subNode, schemaLocation, value)
						}
					}

					if !isExtension {
						if nodeTy == `complexType` {
							subNode.RemoveAttr(`name`)
							item.Parent.RemoveAttr(`type`)
							xmlTool.AddChild(item.Parent, subNode)
						} else if nodeTy == `simpleType` {

							expr := "//xs:restriction"
							list := xmlquery.Find(subNode, expr)

							if len(list) == 0 {
								log.Fatal(`SimpleType define error`)
							}

							item.Parent.RemoveAttr(`type`)
							newAttr := xmlquery.Attr{Name: xml.Name{Local: `type`}, Value: trimNSPrefix(list[0].SelectAttr("base"))}
							item.Parent.Attr = append(item.Parent.Attr, newAttr)
						}
					} else {
						if nodeTy == `complexType` {
							subNode.RemoveAttr(`name`)
							item.Parent.RemoveAttr(`base`)

							// extBaseNode := GetBase(subNode)

							// if extBaseNode == nil {
							// 	log.Fatal(`extension merge error`)
							// }

							currNodeBase := GetBase(item.Parent)

							subNode.Data = "sequence"

							if currNodeBase == nil && item.FirstChild != nil && item.FirstChild.FirstChild == nil {
								//extends empty element
								xmlTool.AddChild(item.Parent, subNode)
							} else {

								if currNodeBase == nil {
									log.Fatal(`extension merge error`)
								}

								t := currNodeBase.FirstChild
								//target := item.Parent.SelectElements("xs:sequence")
								for t != nil {
									temp := t
									t = t.NextSibling
									if parentType != temp.SelectAttr("name") {
										//Prevent infinite recurrsion
										opt.WalkXML(temp, schemaLocation, value)
									}
									xmlTool.AddChild(subNode, temp)
								}

								xmlTool.RemoveFromTree(currNodeBase)
								xmlTool.AddChild(item.Parent, subNode)
							}

						} else if nodeTy == `simpleType` {

							item.Parent.RemoveAttr(`base`)

							targetType := ""
							if isBuildIn {
								targetType = value
							} else {
								item.Parent.RemoveAttr(`base`)
								expr := "//xs:restriction"
								list := xmlquery.Find(subNode, expr)

								if len(list) == 0 {
									log.Fatal(`SimpleType define error`)
								}
								targetType = trimNSPrefix(list[0].SelectAttr("base"))
							}

							it := item.Parent
							for it.Data != "element" {
								it = it.Parent
								if it == nil {
									break
								}
								if strings.Contains(it.Data, "element") {
									newAttr := xmlquery.Attr{Name: xml.Name{Local: `type`}, Value: targetType}
									it.Attr = append(it.Attr, newAttr)
									break
								}
							}

						}
					}
				}
			}
		}
	}
}

func (opt *Options) ReplaceTypesXML(path string) (err error) {
	xmlFile, err := os.Open(path)
	if err != nil {
		fmt.Println(err)
	}
	defer xmlFile.Close()
	t, err := xmlTool.Parse(xmlFile)
	if err != nil {
		return
	}

	opt.WalkXML(t, "", "")

	if t != nil {
		_ = ioutil.WriteFile(path, []byte(t.OutputXML(true)), 0644)
	}

	return nil
}

func (opt *Options) InitSchemaLocation(path string) {
	var xmlFile *os.File
	xmlFile, err := os.Open(opt.FilePath)
	if err != nil {
		return
	}
	defer xmlFile.Close()

	decoder := xml.NewDecoder(xmlFile)

	opt.LocalNameNSMap["base"] = "base"
	opt.NSSchemaLocationMap["base"] = `BaseTypes.xsd`

	for {
		token, _ := decoder.Token()
		if token == nil {
			break
		}

		switch element := token.(type) {
		case xml.StartElement:

			opt.InElement = element.Name.Local
			if opt.InElement == `import` || opt.InElement == `schema` {
				funcName := fmt.Sprintf("On%s", MakeFirstUpperCase(opt.InElement))
				if err = callFuncByName(opt, funcName, []reflect.Value{reflect.ValueOf(element), reflect.ValueOf(opt.ProtoTree)}); err != nil {
					return
				}
			} else {
				break
			}

		case xml.EndElement:
			// funcName := fmt.Sprintf("End%s", MakeFirstUpperCase(element.Name.Local))
			// if err = callFuncByName(opt, funcName, []reflect.Value{reflect.ValueOf(element), reflect.ValueOf(opt.ProtoTree)}); err != nil {
			// 	return
			// }
		default:
		}

	}
}

// Parse reads XML documents and return proto tree for every element in the
// documents by given options. If value of the property extract is false,
// parse will fetch schema used in <import> or <include> statements.
func (opt *Options) Parse(root bool, currFolder, methodName string) (err error) {
	opt.FileDir = currFolder // filepath.Dir(opt.FilePath)
	var fi os.FileInfo
	fi, err = os.Stat(opt.FilePath)
	if err != nil {
		return
	}
	if fi.IsDir() {
		return
	}

	if root {
		opt.InitSchemaLocation(opt.FilePath)
		opt.ReplaceTypesXML(opt.FilePath)
	}

	var xmlFile *os.File
	xmlFile, err = os.Open(opt.FilePath)
	if err != nil {
		return
	}
	defer xmlFile.Close()
	if !opt.Extract {
		opt.ParseFileList[opt.FilePath] = true
		opt.ParseFileMap[opt.FilePath] = opt.ProtoTree
	}
	opt.ProtoTree = make([]interface{}, 0)

	opt.InElement = ""
	opt.CurrentEle = ""
	opt.InGroup = 0
	opt.InUnion = false
	opt.InAttributeGroup = false

	opt.SimpleType = NewStack()
	opt.ComplexType = NewStack()
	opt.Element = NewStack()
	opt.Attribute = NewStack()
	opt.Group = NewStack()
	opt.AttributeGroup = NewStack()
	opt.Choice = NewStack()
	opt.Restrictions = NewStack()

	decoder := xml.NewDecoder(xmlFile)
	decoder.CharsetReader = charset.NewReaderLabel

	for {
		token, _ := decoder.Token()
		if token == nil {
			break
		}

		switch element := token.(type) {
		case xml.StartElement:

			opt.InElement = element.Name.Local
			funcName := fmt.Sprintf("On%s", MakeFirstUpperCase(opt.InElement))
			if err = callFuncByName(opt, funcName, []reflect.Value{reflect.ValueOf(element), reflect.ValueOf(opt.ProtoTree)}); err != nil {
				return
			}

		case xml.EndElement:
			funcName := fmt.Sprintf("End%s", MakeFirstUpperCase(element.Name.Local))
			if err = callFuncByName(opt, funcName, []reflect.Value{reflect.ValueOf(element), reflect.ValueOf(opt.ProtoTree)}); err != nil {
				return
			}
		case xml.CharData:
			if err = opt.OnCharData(string(element), opt.ProtoTree); err != nil {
				return
			}
		default:
		}

	}

	if !opt.Extract {
		opt.ParseFileList[opt.FilePath] = true
		opt.ParseFileMap[opt.FilePath] = opt.ProtoTree
		path := filepath.Join(opt.OutputDir, strings.TrimPrefix(opt.FilePath, opt.InputDir))
		if err := PrepareOutputDir(filepath.Dir(path)); err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		generator := &CodeGenerator{
			Lang:      opt.Lang,
			Package:   opt.Package,
			File:      path,
			ProtoTree: opt.ProtoTree,
			StructAST: map[string]string{},
		}
		funcName := fmt.Sprintf("Gen%s", MakeFirstUpperCase(opt.Lang))
		buff := []reflect.Value{}
		buff = append(buff, reflect.ValueOf(root))
		buff = append(buff, reflect.ValueOf(opt.IsRootTagExport))
		buff = append(buff, reflect.ValueOf(opt.ProjectName))
		buff = append(buff, reflect.ValueOf(methodName))
		if err = callFuncByName(generator, funcName, buff); err != nil {
			return
		}
	}
	return
}

// GetValueType convert XSD schema value type to the build-in type for the
// given value and proto tree.
func (opt *Options) GetValueType(value string, XSDSchema []interface{}) (valueType string, err error) {
	if buildType, ok := getBuildInTypeByLang(trimNSPrefix(value), opt.Lang); ok {
		valueType = buildType
		return
	}
	valueType = getBasefromSimpleType(trimNSPrefix(value), XSDSchema)
	if valueType != trimNSPrefix(value) && valueType != "" {
		return
	}
	if opt.Extract {
		return
	}
	schemaLocation := opt.NSSchemaLocationMap[opt.parseNS(value)]
	if isValidURL(schemaLocation) {
		return
	}
	xsdFile := filepath.Join(opt.FileDir, schemaLocation)
	var fi os.FileInfo
	fi, err = os.Stat(xsdFile)
	if err != nil {
		return
	}
	if fi.IsDir() {
		// extract type of value from include schema.
		valueType = ""
		for include := range opt.IncludeMap {
			parser := NewParser(&Options{
				FilePath:            filepath.Join(opt.FileDir, include),
				OutputDir:           opt.OutputDir,
				Extract:             true,
				Lang:                opt.Lang,
				IncludeMap:          opt.IncludeMap,
				LocalNameNSMap:      opt.LocalNameNSMap,
				NSSchemaLocationMap: opt.NSSchemaLocationMap,
				ParseFileList:       opt.ParseFileList,
				ParseFileMap:        opt.ParseFileMap,
				ProtoTree:           make([]interface{}, 0),
			})
			if parser.Parse(false, opt.InputDir, "") != nil {
				return
			}
			if vt := getBasefromSimpleType(trimNSPrefix(value), parser.ProtoTree); vt != trimNSPrefix(value) {
				valueType = vt
			}
		}
		if valueType == "" {
			valueType = trimNSPrefix(value)
		}
		return
	}

	depXSDSchema, ok := opt.ParseFileMap[xsdFile]
	if !ok {
		parser := NewParser(&Options{
			FilePath:            xsdFile,
			OutputDir:           opt.OutputDir,
			Extract:             false,
			Lang:                opt.Lang,
			IncludeMap:          opt.IncludeMap,
			LocalNameNSMap:      opt.LocalNameNSMap,
			NSSchemaLocationMap: opt.NSSchemaLocationMap,
			ParseFileList:       opt.ParseFileList,
			ParseFileMap:        opt.ParseFileMap,
			ProtoTree:           make([]interface{}, 0),
		})
		if parser.Parse(false, opt.InputDir, "") != nil {
			return
		}
		depXSDSchema = parser.ProtoTree
	}
	valueType = getBasefromSimpleType(trimNSPrefix(value), depXSDSchema)
	if valueType != trimNSPrefix(value) && valueType != "" {
		return
	}
	parser := NewParser(&Options{
		FilePath:            xsdFile,
		OutputDir:           opt.OutputDir,
		Extract:             true,
		Lang:                opt.Lang,
		IncludeMap:          opt.IncludeMap,
		LocalNameNSMap:      opt.LocalNameNSMap,
		NSSchemaLocationMap: opt.NSSchemaLocationMap,
		ParseFileList:       opt.ParseFileList,
		ParseFileMap:        opt.ParseFileMap,
		ProtoTree:           make([]interface{}, 0),
	})
	if parser.Parse(false, opt.InputDir, "") != nil {
		return
	}
	valueType = getBasefromSimpleType(trimNSPrefix(value), parser.ProtoTree)
	return
}
